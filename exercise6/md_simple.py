"""
Simple Molecular Dynamics code for course Computational Physics 2

Problem 2:
- Make the code to work and solve H2 using the Morse potential.
- Modify and comment especially at parts where it reads "# ADD"
- Follow instructions on ex6.pdf


Not required, but could be interesting at some point:
- Additionally you could be interested in adding other observables,
such as calculation of the temperature, distance between consituents, 
and heat capacity, however, this is not required for the exercises.
"""

"""
Added code to the parts that said #ADD but something about the added code is off.
Energy values received with the code are wrong but I couldn't find the error in 
the added code.
"""



from numpy import *
from matplotlib.pyplot import *

class Atom:
    def __init__(self,index,mass,dt,dims):
        self.index = index
        self.mass = mass
        self.dt = dt
        self.dims = dims
        self.LJ_epsilon = None
        self.LJ_sigma = None
        self.R = None
        self.v = None
        self.force = None

    def set_LJ_parameters(self,LJ_epsilon,LJ_sigma):
        self.LJ_epsilon = LJ_epsilon
        self.LJ_sigma = LJ_sigma

    def set_position(self,coordinates):
        self.R = coordinates
        
    def set_velocity(self,velocity):
        self.v = velocity

    def set_force(self,force):
        self.force = force

class Observables:
    def __init__(self):
        self.E_kin = []
        self.E_pot = []
        self.distance = []
        self.Temperature = []

def calculate_energetics(atoms):
    N = len(atoms)
    V = 0.0
    E_kin = 0.0
    # ADD calculation of kinetic and potential energy
    for i in range(N):
        #E_kin += 1/2*atoms[i].mass*atoms[i].v**2
        E_kin += 1/2*atoms[i].mass*sqrt(atoms[i].v.dot(atoms[i].v))**2
    V += pair_potential(atoms[0],atoms[1])
    return E_kin, V

def calculate_force(atoms):
    # ADD comments, e.g., what are the elements of the return function F
    # 
    # Calculating the net force on particles.
    # The elements in the output array are the net forces that the atoms 
    # in the input array feel in the same order
    N = len(atoms)
    ij_map = zeros((N,N),dtype=int)
    Fs = []
    ind = 0
    for i in range(0,N-1):
        for j in range(i+1,N):
            Fs.append(pair_force(atoms[i],atoms[j]))
            ij_map[i,j] = ind
            ij_map[j,i] = ind
            ind += 1
    F = []
    for i in range(N):
        f = zeros(shape=shape(atoms[i].R))
        for j in range(N):
            ind = ij_map[i,j]
            if i<j:
                f += Fs[ind]
            elif i>j:
                f -= Fs[ind]
        F.append(f)
    F = array(F)
    return F

def pair_force(atom1,atom2):
    return Morse_force(atom1,atom2)

def pair_potential(atom1,atom2):
    return Morse_potential(atom1,atom2)

def Morse_potential(atom1,atom2):
    # H2 parameters given here
    De = 0.1745
    re = 1.40
    a = 1.0282
    r = atom1.R-atom2.R
    dr = sqrt(sum(r**2))
    # ADD comments and calculation of Morse potential
    # u_M(r) = De*(1-e^(-a*(dr-re)))^2 - De
    # De is dissociation energy
    # re is the bond length
    # a=sqrt(k/(2De)) where k is a force constant
    u_M = De*(1-exp(-a*(dr-re)))**2 - De
    return u_M

def Morse_force(atom1,atom2):
    # H2 parameters
    De = 0.1745
    re = 1.40
    a = 1.0282
    r = atom1.R-atom2.R
    dr = sqrt(sum(r**2))
    # ADD comments and calculation of Morse force
    # - calculate the form for the force analytically, and evaluate it here
    # F_M = -d/dr(u_M)
    #
    #
    F_M = -2*De*(1-exp(-a*(dr-re)))*a*exp(-a*(dr-re))
    return F_M

def lennard_jones_potential(atom1,atom2):
    # Lorentz-Berthelot mixing for epsilon and sigma
    epsilon = sqrt(atom1.LJ_epsilon*atom2.LJ_epsilon)
    sigma = (atom1.LJ_sigma+atom2.LJ_sigma)/2
    # If interested one could add the calculation of LJ potential here
    #
    #
    return 

def lennard_jones_force(atom1,atom2):
    # Lorentz-Berthelot mixing for epsilon and sigma
    epsilon = sqrt(atom1.LJ_epsilon*atom2.LJ_epsilon)
    sigma = (atom1.LJ_sigma+atom2.LJ_sigma)/2
    # If interested one could add the calculation of LJ force here
    #
    #
    return 

def velocity_verlet_update(atoms):
    # ADD comments and integration algorithm according to function name
    # velocity verlet integration algorithm used for updating
    # positions and velocities
    dt = atoms[0].dt
    dt2 = dt**2
    for i in range(len(atoms)):
        # calculating new positions
        atoms[i].R += dt*atoms[i].v + dt2*1/(2*atoms[i].mass)*atoms[i].force # ADD position update (notice += )
    # calculating new force and saving it as a variable
    # so it does not need to be calculated again
    Fnew = calculate_force(atoms)
    for i in range(len(atoms)):
        # calculating new velocities
        atoms[i].v += dt*1/(2*atoms[i].mass)*(Fnew[i] + atoms[i].force)# ADD velocity update (notice += )
        atoms[i].force = Fnew[i] # update force
    return atoms
    
def initialize_positions(atoms):
    # diatomic case
    atoms[0].set_position(array([-0.8,0.0,0.0]))
    atoms[1].set_position(array([0.7,0.0,0.0]))

def initialize_velocities(atoms):
    # diatomic case 
    dims = atoms[0].dims
    kB=3.16e-6 # in hartree/Kelvin
    for i in range(len(atoms)):
        v_max = sqrt(3.0/atoms[i].mass*kB*10.0)
        atoms[i].set_velocity(array([1.0,0.0,0.0])*v_max)
    atoms[1].v = -1.0*atoms[0].v

def initialize_force(atoms):
    F=calculate_force(atoms)
    for i in range(len(atoms)):
        atoms[i].set_force(F[i])

def Temperature():
    # Boltzmann constant in Hartree/Kelvin
    kB = 3.16e-6
    # you could be interested in adding the calculation of temperature
    #
    #
    return 

def calculate_observables(atoms,observables):
    # calculating kinectic and potential energies of atoms
    E_k, E_p = calculate_energetics(atoms)
    observables.E_kin.append(E_k)
    observables.E_pot.append(E_p)
    # Here you could add calculation of other observables
    #
    #
    #
    return observables

def main():
    N_atoms = 2
    dims = 3
    dt = 0.1
    mass = 1836.0

    # Initialize atoms
    atoms = []    
    for i in range(N_atoms):
        atoms.append(Atom(i,mass,dt,dims))
        # LJ not used, but the next line shows a way to set LJ parameters 
        atoms[i].set_LJ_parameters(0.1745,1.25) 

    # Initialize observables
    observables = Observables()

    # Initialize positions, velocities, and forces
    initialize_positions(atoms)
    initialize_velocities(atoms)
    initialize_force(atoms)

    for i in range(100000):
        atoms = velocity_verlet_update(atoms)
        if ( i % 10 == 0):
            observables = calculate_observables(atoms,observables)            

    # Print energies
    E_kin = array(observables.E_kin)
    E_pot = array(observables.E_pot)
    print('E_kin',mean(E_kin))
    print('E_pot',mean(E_pot))
    print('E_tot',mean(E_kin)+mean(E_pot))

    print(E_kin)
    print(E_pot)
    figure()
    plot(E_kin+E_pot)
    xlabel("Time")
    ylabel("E_tot")
    text(2000,0.1,r'E_kin={0:.6f}'.format(mean(E_kin)))
    text(2000,0.0,r'E_pot={0:.6f}'.format(mean(E_pot)))
    show()
    
    

if __name__=="__main__":
    main()
        
