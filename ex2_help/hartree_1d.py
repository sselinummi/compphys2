#! /usr/bin/env python3

"""
Hartree code for N-electron 1D harmonic quantum dot

- Related to Computational Physics 2
- Test case should give values very close to:

    Total energy      11.502873299221452
    Kinetic energy    3.6221136067112054
    Potential energy  7.880759692510247

    Density integral  4.0

- Job description: 
  -- Problem 2: add/fill needed functions, details and especially comments
  -- Problem 3: Modify parameters for calculating a different system
                as described in the problem setup
  -- Problem 4: Include input and output as text file
  -- Problem 5: Include input and output as DHF5 file

"""

import numpy as np
import matplotlib.pyplot as plt
import scipy.sparse as sp
import scipy.sparse.linalg as sla
from scipy.integrate import simps
import h5py
import os

def hartree_potential(ns,x):
    """ 
    Hartree potential using Simpson integration 
    """
    Vhartree=0.0*ns
    for ix in range(len(x)):
        r = x[ix]
        f = 0.0*x
        for ix2 in range(len(x)):
            rp = x[ix2]
            f[ix2]=ns[ix2]*ee_potential(r-rp)
        Vhartree[ix]=simps(f,x)
    return Vhartree

def ee_potential(x):
    global ee_coef
    """ 1D electron-electron interaction """
    return ee_coef[0]/np.sqrt(x**2+ee_coef[1])

def ext_potential(x,m=1.0,omega=1.0):
    """ 1D harmonic quantum dot """
    return 0.5*m*omega**2*x**2

def density(psis):
    ns=np.zeros((len(psis[0]),))
    for i in range(len(psis)):
        ns+=abs(psis[i])**2
    return ns
    
def initialize_density(x,dx,normalization=1):
    rho=np.exp(-x**2)
    A=simps(rho,x)
    return normalization/A*rho

def check_convergence(Vold,Vnew,threshold):
    difference_ = np.amax(abs(Vold-Vnew))
    print('  Convergence check:', difference_)
    converged=False
    if difference_ <threshold:
        converged=True
    return converged

def diagonal_energy(T,orbitals,x):
    """ 
    Calculate diagonal energy
    (using Simpson)
    """
    Tt=sp.csr_matrix(T)
    E_diag=0.0
    
    for i in range(len(orbitals)):
        evec=orbitals[i]
        E_diag+=simps(evec.conj()*Tt.dot(evec),x)
    return E_diag

def offdiag_potential_energy(orbitals,x):
    """ 
    Calculate off-diagonal energy
    (using Simpson)
    """
    U = 0.0
    for i in range(len(orbitals)-1):
        for j in range(i+1,len(orbitals)):
            fi = 0.0*x
            for i1 in range(len(x)):
                fj = 0.0*x
                for j1 in range(len(x)):
                    fj[j1]=abs(orbitals[i][i1])**2*abs(orbitals[j][j1])**2*ee_potential(x[i1]-x[j1])
                fi[i1]=simps(fj,x)
            U+=simps(fi,x)
    return U

def save_ns_in_ascii(ns,filename):
    s=ns.shape
    f=open(filename+'.txt','w')
    for ix in range(s[0]):
        f.write('{0:12.8f}\n'.format(ns[ix]))
    f.close()
    f=open(filename+'_shape.txt','w')
    f.write('{0:5}'.format(s[0]))
    f.close()
    
def load_ns_from_ascii(filename):
    f=open(filename+'_shape.txt','r')
    for line in f:
        s=array(line.split(),dtype=int)
    f.close()
    ns=np.zeros((s[0],))
    d=loadtxt(filename+'.txt')
    k=0
    for ix in range(s[0]):
        ns[ix]=d[k]
        k+=1
    return ns

def save_data_to_hdf5_file(fname,orbitals,density,N_e,occ,grid,ee_coefs):
    return

def calculate_SIC(orbitals,x):
    V_SIC = []
    for i in range(len(orbitals)):
        V_SIC.append(-hartree_potential(abs(orbitals[i])**2,x))
    return V_SIC
            
def normalize_orbital(evec,x):
    return evec/np.sqrt(simps(abs(evec)**2,x))

def kinetic_hamiltonian(x):
    grid_size = x.shape[0]
    dx = x[1] - x[0]
    dx2 = dx**2
    
    H0 = sp.diags(
        [
            -0.5 / dx2 * np.ones(grid_size - 1),
            1.0 / dx2 * np.ones(grid_size),
            -0.5 / dx2 * np.ones(grid_size - 1)
        ],
        [-1, 0, 1])
    return H0

def save_data(density,orbitals):
    np.savetxt('density.txt',density)
    for i in range(len(orbitals)):
        np.savetxt('orbital_{}.txt'.format(i),orbitals[i])

def load_data(Norbs):
    density = np.loadtxt('density.txt')
    orbitals = []
    for i in range(Norbs):
        orbitals.append(np.loadtxt('orbital_{}.txt'.format(i)))
    return density,orbitals
    

def main():
    global ee_coef
    # e-e potential parameters [strenght, smoothness]
    ee_coef = [1.0, 1.0]
    # 1D occupations each orbital can have max 2, i.e., spin up and spin down
    # e.g., occ = [0,0,1,1] means lowest up and down, next lowest up and down
    #       occ = [0,1,2,3] means all spin up
    
    #occ = [0,0,1,1,2,2] # P5 S=0
    #occ = [0,1,2,3,4,5] # P5 S=3
    #occ = [0,1,2,3,4,5,6,7] # P5 S=4
    #occ = [0,0,1,1,2,2,3,3] # P5 S=0
    occ = [0,1,2,3] # test case (P1)

    # number of electrons
    N_e = len(occ)

    # grid
    x=np.linspace(-4,4,120)
    # threshold
    threshold=1.0e-8
    # mixing value
    mix_alpha=0.2
    # maximum number of iterations
    maxiters = 100



    dx = x[1]-x[0]
    T = kinetic_hamiltonian(x)
    Vext = ext_potential(x)

    # READ in density / orbitals / etc.
    if os.path.isfile('density.txt'):
        #ns=load_ns_from_ascii('density')
        ns,orbitals = load_data(N_e)
        print('\nCalculating initial state')
        Vhartree=hartree_potential(ns,x)
        VSIC=calculate_SIC(orbitals,x)
    else:
        ns=initialize_density(x,dx,N_e)
        print('\nCalculating initial state')
        Vhartree=hartree_potential(ns,x)
        VSIC=[]
        for i in range(N_e):
            VSIC.append(ns*0.0)

    print('Density integral        ', simps(ns,x))
    print(' -- should be close to  ', N_e)
    
 
    Veff=sp.diags(Vext+Vhartree,0)
    H=T+Veff
    for i in range(maxiters):
        print('\n\nIteration #{0}'.format(i))
        orbitals=[]
        for i in range(N_e):
            print('  Calculating orbitals for electron ', i+1)
            eigs, evecs = sla.eigs(H+sp.diags(VSIC[i],0), k=N_e, which='SR')
            eigs = np.real(eigs)
            evecs = np.real(evecs)
            print('    eigenvalues', eigs)
            evecs[:,occ[i]]=normalize_orbital(evecs[:,occ[i]],x)
            orbitals.append(evecs[:,occ[i]])
        Veff_old = 1.0*Veff
        ns=density(orbitals)
        Vhartree=hartree_potential(ns,x)
        VSIC=calculate_SIC(orbitals,x)
        Veff_new=sp.diags(Vext+Vhartree,0)
        if check_convergence(Veff_old,Veff_new,threshold):
            break
        else:
            Veff=(1.0-mix_alpha)*Veff_new+mix_alpha*Veff_old
            H = T+Veff

    print('\n\n')
    off = offdiag_potential_energy(orbitals,x)
    E_kin = diagonal_energy(T,orbitals,x)
    E_pot = diagonal_energy(sp.diags(Vext,0),orbitals,x) + off
    E_tot = E_kin + E_pot
    print('Total energy     ', E_tot)
    print('Kinetic energy   ', E_kin)
    print('Potential energy ', E_pot) 
    print('\nDensity integral ', simps(ns,x))

    # WRITE OUT density / orbitals / energetics / etc.
    #save_ns_in_ascii(ns,'density')
    save_data(ns,orbitals)    

    #plt.rcParams['text.usetex'] = True
    plt.rcParams['font.size'] = 12
    plt.rcParams['legend.handlelength'] = 2 
    plt.rcParams['legend.numpoints'] = 1
    plt.plot(x,abs(ns))
    plt.xlabel(r'$x$ (a.u.)')
    plt.ylabel(r'$n(x)$ (1/a.u.)')
    plt.title(r'$N$-electron density for $N={0}$'.format(N_e))
    plt.show()

if __name__=="__main__":
    main()
