"""
Simple Monte Carlo for Ising model

Related to course Computational Physics 2 at TAU

Problem 1:
- Make the code work, that is, include code to where it reads "# ADD"
- Comment the parts with "# ADD" and make any additional comments you 
  think could be useful for yourself later.
- Follow the assignment from ex5.pdf.

Problem 2:
- Add observables: heat capacity, magnetization, magnetic susceptibility
- Follow the assignment from ex5.pdf.

Problem 3:
- Look at the temperature effects and locate the phase transition temperature.
- Follow the assignment from ex5.pdf.

"""


from numpy import *
from matplotlib.pyplot import *

class Walker:
    def __init__(self,*args,**kwargs):
        self.spin = kwargs['spin']
        self.nearest_neighbors = kwargs['nn']
        self.sys_dim = kwargs['dim']
        self.coords = kwargs['coords']

    def w_copy(self):
        return Walker(spin=self.spin.copy(),
                      nn=self.nearest_neighbors.copy(),
                      dim=self.sys_dim,
                      coords=self.coords.copy())
    

def Energy(Walkers):
    # Calculate the energy of walkers
    E = 0.0
    J = 4.0 # given in units of k_B
    # ADD calculation of energy
    for walker in Walkers:
        E += site_Energy(Walkers, walker)
    # returned energy divided by 2 because the usage of site_Energy
    # calculates all the sites twice
    return E/2

def magn_moment(Walkers):
    # Calculate the magnetic moment of walkers
    m = 0.0
    for walker in Walkers:
        m += walker.spin
    return m

def site_Energy(Walkers,Walker):
    # Calculate the energy of a walker causes by its nearest neighbours
    E = 0.0
    J = 4.0 # given in units of k_B
    for k in range(len(Walker.nearest_neighbors)):
        j = Walker.nearest_neighbors[k]
        E += -J*Walker.spin*Walkers[j].spin
    return E

def ising(Nblocks,Niters,Walkers,beta):
    # Solving the Ising model for a ferromagnet with nearest neighbour
    # interactions using classical Monte Carlo
    M = len(Walkers)
    Eb = zeros((Nblocks,))
    Eb2 = zeros((Nblocks,))
    mb = zeros((Nblocks,))
    mb2 = zeros((Nblocks,))
    C_V = zeros((Nblocks,))
    X = zeros((Nblocks,))
    Accept=zeros((Nblocks,))
    AccCount=zeros((Nblocks,))

    obs_interval = 5
    for i in range(Nblocks):
        EbCount = 0
        for j in range(Niters):
            site = int(random.rand()*M)

            # old spin
            s_old = 1.0*Walkers[site].spin
 
            # energy with old spin
            E_old = site_Energy(Walkers,Walkers[site])

            # ADD selection of new spin to variable s_new
            s_new = random.choice([-0.5,0.5])
            #print(s_new)
            #
            #
            #
            #

            Walkers[site].spin = 1.0*s_new

            # energy with new spin
            E_new = site_Energy(Walkers,Walkers[site])

            # ADD Metropolis Monte Carlo
            # for deciding if the spin will be flipped or not
            q_R_Rp = exp(-(E_new-E_old)*beta)
            A_RtoRp = min(1.0,q_R_Rp)
            if (A_RtoRp > random.rand()):
                Accept[i] += 1.0
            else:
                Walkers[site].spin = 1.0*s_old
            AccCount[i] += 1
            #
            #
            #
            #
            #
            #
            #
            #

            # Calculating observables
            if j % obs_interval == 0:
                E_tot = Energy(Walkers)
                Eb[i] += E_tot
                Eb2[i] += E_tot**2
                mag = magn_moment(Walkers)
                mb[i] += mag
                mb2[i] += mag**2
                EbCount += 1
            
        Eb[i] /= EbCount
        Eb2[i] /= EbCount
        mb[i] /= EbCount
        mb2[i] /= EbCount
        Accept[i] /= AccCount[i]
        print('Block {0}/{1}'.format(i+1,Nblocks))
        print('    E   = {0:.5f}'.format(Eb[i]))
        print('    M   = {0:.5f}'.format(mb[i]))
        print('    C_V   = {0:.5f}'.format(C_V[i]))
        print('    X   = {0:.5f}'.format(X[i]))
        print('    Acc = {0:.5f}'.format(Accept[i]))


    return Walkers, Eb, Eb2, mb, mb2, Accept


def main():
    Walkers=[]

    dim = 2
    grid_side = 10
    grid_size = grid_side**dim
    
    # Ising model nearest neighbors only
    mapping = zeros((grid_side,grid_side),dtype=int) # mapping
    inv_map = [] # inverse mapping
    ii = 0
    for i in range(grid_side):
        for j in range(grid_side):
            mapping[i,j]=ii
            inv_map.append([i,j])
            ii += 1
 

    # ADD comment
    # Add walkers into a lattice with the knowledge of their nearest
    # neighbors
    for i in range(grid_side):
        for j in range(grid_side):
            j1=mapping[i,(j-1) % grid_side]
            j2=mapping[i,(j+1) % grid_side]
            i1=mapping[(i-1) % grid_side,j]
            i2=mapping[(i+1) % grid_side,j]
            Walkers.append(Walker(spin=0.5,
                                  nn=[j1,j2,i1,i2],
                                  dim=dim,
                                  coords = [i,j]))
 
    
    Nblocks = 200
    Niters = 1000
    eq = 20 # equilibration "time"
    # temperature grid
    T_grid = linspace(0.5,6.0,20)
    Ebs=[]
    mbs=[]
    C_Vs=[]
    Xs=[]
    # looping through all of the temperatures
    for Ti in T_grid:
        T = Ti
        beta = 1.0/T
        """
        Notice: Energy is measured in units of k_B, which is why
                beta = 1/T instead of 1/(k_B T)
        """
        Walkers, Eb, Eb2, mb, mb2, Acc = ising(Nblocks,Niters,Walkers,beta)
        # Calculate heat capacity
        C_Vs.append((mean(Eb2) - mean(Eb)**2)/T**2/grid_size)
        # Calculate magnetic susceptibility
        Xs.append((mean(mb2) - mean(mb)**2)/T/grid_size)
        # Energy and magnetization per spin
        mbs.append(mean(mb/grid_size))
        Ebs.append(mean(Eb/grid_size))

    figure()
    plot(Ebs)
    ylabel('Energy per spin')
    xlabel('Temperature')
    Eb = Eb[eq:]
    figure()
    plot(mbs)
    ylabel('Magnetization per spin')
    xlabel('Temperature')
    figure()
    plot(C_Vs)
    ylabel('Heat capacity per spin')
    xlabel('Temperature')
    figure()
    plot(Xs)
    ylabel('Susceptibility per spin')
    xlabel('Temperature')
    #print('Ising total energy: {0:.5f} +/- {1:0.5f}'.format(mean(Eb), std(Eb)/sqrt(len(Eb))))
    #print('Variance to energy ratio: {0:.5f}'.format(abs(var(Eb)/mean(Eb)))) 
    #print('Heat capacity: {0:.5f}'.format(C_V)) 
    #print('Magnetization: {0:.5f}'.format(mean(mb))) 
    #print('Magnetic susceptibility: {0:.5f}'.format(X)) 
    show()

if __name__=="__main__":
    main()
        
