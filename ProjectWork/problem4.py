"""
Simulating the Ising model of a ferromagnet in a regular 2D grid with
Monte Carlo. Interactions can be switched from having only the nearest neighbor
interactions to having both the nearest and the next nearest neighbor
interactions. Energy, heat capacity, magnetization and magnetic susceptibility
can be calculated as a function of temperature (with constant lattice size) or
they can be calculated as a function of lattice side length 
(with constant temperature).
"""


from numpy import *
from matplotlib.pyplot import *
from scipy.special import erf

class Walker:
    def __init__(self,*args,**kwargs):
        self.spin = kwargs['spin']
        self.nearest_neighbors = kwargs['nn']
        self.next_nearest_neighbors = kwargs['nnn']
        self.sys_dim = kwargs['dim']
        self.coords = kwargs['coords']

    def w_copy(self):
        return Walker(spin=self.spin.copy(),
                      nn=self.nearest_neighbors.copy(),
                      nnn=self.next_nearest_neighbors.copy(),
                      dim=self.sys_dim,
                      coords=self.coords.copy())
    

def Energy(Walkers):
    # Calculate the energy of walkers
    E = 0.0
    J1 = 4.0 # given in units of k_B, for nearest neighbors
    J2 = 1.0 # given in units of k_B, for next nearest neighbors
    for i in range(len(Walkers)):
        for k in range(len(Walkers[i].nearest_neighbors)):
            j = Walkers[i].nearest_neighbors[k]
            E += -J1*Walkers[i].spin*Walkers[j].spin
        if next_nearest == True:
            for k in range(len(Walkers[i].next_nearest_neighbors)):
                j = Walkers[i].next_nearest_neighbors[k]
                E += -J2*Walkers[i].spin*Walkers[j].spin
    # returned energy is divided by two because every site was calculated twice
    return E/2

def site_Energy(Walkers,Walker):
    # Calculate the energy of a walker caused by its nearest (and next nearset)
    # neighbours
    E = 0.0
    J1 = 4.0 # given in units of k_B, for nearest neighbors
    J2 = 1.0 # given in units of k_B, for next nearest neighbors
    for k in range(len(Walker.nearest_neighbors)):
        j = Walker.nearest_neighbors[k]
        E += -J1*Walker.spin*Walkers[j].spin
    if next_nearest == True:
        for k in range(len(Walker.next_nearest_neighbors)):
            j = Walker.next_nearest_neighbors[k]
            E += -J2*Walker.spin*Walkers[j].spin
    return E

def magnetization(Walkers):
    # Calculate magnetization of walkers
    m = 0.0
    for i in range(len(Walkers)):
        m += Walkers[i].spin
    return m

def ising(Nblocks,Niters,Walkers,beta):
    # Solving the Ising model for a ferromagnet with nearest (and next
    # nearest) neighbour interactions using classical Monte Carlo
    M = len(Walkers)
    Eb = zeros((Nblocks,))
    Eb2 = zeros((Nblocks,))
    mb = zeros((Nblocks,))
    mb2 = zeros((Nblocks,))
    Accept=zeros((Nblocks,))
    AccCount=zeros((Nblocks,))

    obs_interval = 5
    for i in range(Nblocks):
        EbCount = 0
        for j in range(Niters):
            site = int(random.rand()*M)

            s_old = 1.0*Walkers[site].spin
 
            E_old = site_Energy(Walkers,Walkers[site])

            # selection of new spin to variable s_new
            if random.rand()>0.5:
                s_new = 0.5
            else:
                s_new = -0.5
            Walkers[site].spin = 1.0*s_new

            # energy with new spin
            E_new = site_Energy(Walkers,Walkers[site])

            # Metropolis Monte Carlo
            # for deciding if the spin will be flipped or not
            deltaE = E_new-E_old
            q_s_sp = exp(-beta*deltaE)
            A_s_sp = min(1.0,q_s_sp)
            if (A_s_sp > random.rand()):
                Accept[i] += 1.0
            else:
                Walkers[site].spin=1.0*s_old
            AccCount[i] += 1

            # Calculating observables
            if j % obs_interval == 0:
                E_tot = Energy(Walkers)
                Eb[i] += E_tot
                Eb2[i] += E_tot**2
                mag = magnetization(Walkers)
                mb[i] += mag
                mb2[i] += mag**2
                EbCount += 1
            
        Eb[i] /= EbCount
        Eb2[i] /= EbCount
        mb[i] /= EbCount
        mb2[i] /= EbCount
        Accept[i] /= AccCount[i]
        print('Block {0}/{1}'.format(i+1,Nblocks))
        print('    E   = {0:.5f}'.format(Eb[i]))
        print('    Acc = {0:.5f}'.format(Accept[i]))


    return Walkers, Eb, Eb2, mb, mb2, Accept


def main():
    Walkers=[]

    # Includes the next nearest neighbor interactions if true
    global next_nearest
    next_nearest = False
    
    # For finite size effect case (only nearest neighbors case).
    # If true, loops through different grid sides while keeping the temperature
    # constant
    # If false, uses default grid side (10) and loops through different
    # temperatures
    finite_size_effect = True

    
    dim = 2
    if finite_size_effect == True:
        grid_sides = [4,8,16,32]
        #grid_sides = [4]
    else:
        grid_sides = [10] # switching values 4,8,16,32 for seeing the finite size effect
    
    # arrays for storing finite size effect data
    Es = []
    Cvs = []
    Ms = []
    Chis = []
    for grid_side in grid_sides:
        Walkers=[]
        grid_size = grid_side**dim
        
        # Ising model nearest neighbors only
        mapping = zeros((grid_side,grid_side),dtype=int)
        inv_map = []
        ii = 0
        for i in range(grid_side):
            for j in range(grid_side):
                mapping[i,j]=ii
                inv_map.append([i,j])
                ii += 1
        
        # Add walkers into a lattice with the knowledge of their nearest
        # neighbors (and next nearest neighbors)
        for i in range(grid_side):
            for j in range(grid_side):
                j1=mapping[i,(j-1) % grid_side]
                j2=mapping[i,(j+1) % grid_side]
                i1=mapping[(i-1) % grid_side,j]
                i2=mapping[(i+1) % grid_side,j]
                if next_nearest == True:
                    j11=mapping[(i-1) % grid_side,(j-1) % grid_side]
                    j22=mapping[(i-1) % grid_side,(j+1) % grid_side]
                    i11=mapping[(i+1) % grid_side,(j-1) % grid_side]
                    i22=mapping[(i+1) % grid_side,(j+1) % grid_side]
                    Walkers.append(Walker(spin=0.5,
                                        nn=[j1,j2,i1,i2],
                                        nnn=[j11,j22,i11,i22],
                                        dim=dim,
                                        coords = [i,j]))
                else:
                    Walkers.append(Walker(spin=0.5,
                                        nn=[j1,j2,i1,i2],
                                        nnn=[],
                                        dim=dim,
                                        coords = [i,j]))
    
        
        Nblocks = 200
        Niters = 2000

        # deciding if a grid or a constant value will be used for temperature
        if finite_size_effect == True:
            T = array([4.0]) # constant temperature for the finite size effect case
        else:
            T = linspace(0.5,6,20)
        
        Edata = []
        Edata2 = []
        Mdata = []
        Mdata2 = []
        eq = 20
        for i in range(len(T)):
            beta = 1.0/T[i]
            Walkers, Eb, Eb2, mb, mb2, Acc = ising(Nblocks,Niters,Walkers,beta)
            
            E_and_err = array([mean(Eb[eq:]), std(Eb[eq:])/sqrt(len(Eb[eq:]))])
            E2_and_err = array([mean(Eb2[eq:]), std(Eb2[eq:])/sqrt(len(Eb2[eq:]))])
            Edata.append(E_and_err)
            Edata2.append(E2_and_err)
            M_and_err = array([mean(mb[eq:]), std(mb[eq:])/sqrt(len(mb[eq:]))])
            M2_and_err = array([mean(mb2[eq:]), std(mb2[eq:])/sqrt(len(mb2[eq:]))])
            Mdata.append(M_and_err)
            Mdata2.append(M2_and_err)

        if finite_size_effect == False:
            Edata=array(Edata)
            Edata2=array(Edata2)
            Mdata=array(Mdata)
            Mdata2=array(Mdata2)
            # Notice: T_c_exact = 2.27
            fig = figure()
            # energy
            ax = fig.add_subplot(221)
            ax.errorbar(T,Edata[:,0]/grid_size,Edata[:,1]/grid_size)
            ax.set_ylabel('Energy per spin')
            ax.set_xlabel('Temperature')
            # heat capacity
            ax2 = fig.add_subplot(222)
            Cv = (Edata2[:,0]-Edata[:,0]**2)/T**2/grid_size
            ax2.plot(T,Cv,'-o')
            ax2.set_ylabel('Heat Capacity per spin')
            ax2.set_xlabel('Temperature')
            # magnetization
            ax3 = fig.add_subplot(223)
            ax3.errorbar(T,Mdata[:,0]/grid_size,Mdata[:,1]/grid_size)
            ax3.set_ylabel('Magnetization per spin')
            ax3.set_xlabel('Temperature')
            # susceptibility
            ax4 = fig.add_subplot(224)
            chi = (Mdata2[:,0]-Mdata[:,0]**2)/T/grid_size
            ax4.plot(T,chi,'-o')
            ax4.set_ylabel('Susceptibility per spin')
            ax4.set_xlabel('Temperature')

            if next_nearest == True:
                fig.suptitle("The nearest and next nearest neighbor interactions (J1 = 4, J2 = 1)", fontsize=14)
            else:
                fig.suptitle("The nearest neighbor interactions (J1 = 4)", fontsize=14)
            print('Ising total energy: {0:.5f} +/- {1:0.5f}'.format(mean(Eb), std(Eb)/sqrt(len(Eb))))
            print('Variance to energy ratio: {0:.5f}'.format(abs(std(Eb)/mean(Eb)))) 
            show()
        else:
            Edata=array(Edata)
            Edata2=array(Edata2)
            Mdata=array(Mdata)
            Mdata2=array(Mdata2)
            Es.append(Edata[0,0]/grid_size)
            Cvs.append((Edata2[0,0]-Edata[0,0]**2)/T[0]**2/grid_size)
            Ms.append(Mdata[0,0]/grid_size)
            Chis.append((Mdata2[0,0]-Mdata[0,0]**2)/T[0]/grid_size)

        # delete walkers at the end of the grid_side loop
        for i in range(len(Walkers)-1,-1,-1):
            del Walkers[i]

    if finite_size_effect == True:
        fig = figure()
        fig.suptitle("Finite size effects (T = 4.0 K)", fontsize=12)
        # energy
        ax = fig.add_subplot(221)
        ax.plot(grid_sides,Es,'-o')
        ax.set_ylabel('Energy per spin')
        ax.set_xlabel('N')
        # heat capacity
        ax2 = fig.add_subplot(222)
        ax2.plot(grid_sides,Cvs,'-o')
        ax2.set_ylabel('Heat Capacity per spin')
        ax2.set_xlabel('N')
        # magnetization
        ax3 = fig.add_subplot(223)
        ax3.plot(grid_sides,Ms,'-o')
        ax3.set_ylabel('Magnetization per spin')
        ax3.set_xlabel('N')
        # susceptibility
        ax4 = fig.add_subplot(224)
        ax4.plot(grid_sides,Chis,'-o')
        ax4.set_ylabel('Susceptibility per spin')
        ax4.set_xlabel('N')
        show()

if __name__=="__main__":
    main()
        
