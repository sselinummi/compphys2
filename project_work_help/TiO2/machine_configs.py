#! /usr/bin/env python3
from nexus import job

"""
Look for ADD parts in this file, that is, something needs to be modified.
"""

def general_configs(machine):
    if machine=='puhti':
        jobs = get_puhti_configs()
    else:
        print('Using Puhti as defaul machine')
        jobs = get_puhti_configs()
    return jobs

def get_puhti_configs():
    scf_presub = '''
module load intel/19.0.4
module load hpcx-mpi/2.4.0
module load intel-mkl/2019.0.4
module load StdEnv
module load hdf5
module load cmake/3.18.2
module load git
    '''

    qe='pw.x'
    pp_app = '...' # !!!!!! ADD: what's your pp_app executable !!!!!!

    # 4 processes for now
    # scf and pp below are OK... for the heavier runs you could have, e.g., cores=10, minutes=30
    scf  = job(cores=4,minutes=10,user_env=False,presub=scf_presub,app=qe)
    pp = job(cores=4,minutes=10,user_env=False,presub=scf_presub,app=pp_app)

    # !!!!!! ADD: need to add pp option to jobs below also !!!!!!
    jobs = {'scf' : scf}

    return jobs
