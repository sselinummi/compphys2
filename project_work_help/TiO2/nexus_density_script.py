#! /usr/bin/env python3
"""
Could be helpful, but not necessary to use
"""

from numpy import *
import os
from fileio import XsfFile

fname = 'TiO2_density.xsf'
       
s = XsfFile(fname)

density=s.data[3]['3d_pwscf']['datagrid_3d_unknown'].values.copy()

# here you can modify density if you want
# you could, e.g., read two densities in and take the difference
# below you can set, e.g., the difference into the dataset and export
# it into a new file

# set density
s.data[3]['3d_pwscf']['datagrid_3d_unknown'].values = density

# export xsf file with different name
fname_out = 'out_density.xsf'
s.write(fname_out)
