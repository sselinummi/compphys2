"""
Script for problem 3 plotting
"""

from numpy import *
import matplotlib.pyplot as plt
from mpl_toolkits import mplot3d

def main():
    """
    Plotting electron densities and total energies for a 2D many-electron
    semiconductor quantum dot in cases N=6 S=3 and N=6 S=0. Plotted values 
    were calculated at CSC using Path Integral Monte Carlo algorithm.
    """
    
    x1=[]
    y1=[]
    z1=[]
    f = open("ag_density.dat")
    counter=0
    for line in f:  
        if line.strip() != '':  
            counter+=1      
            values=line.strip().split()
            #print(values)
            x1.append(float(values[0]))
            y1.append(float(values[1]))
            z1.append(float(values[2]))       
    f.close()

    x2=[]
    y2=[]
    z2=[]
    f = open("ag_density2.dat")
    for line in f:
        if line.strip() != '':        
            values=line.strip().split()
            x2.append(float(values[0]))
            y2.append(float(values[1]))
            z2.append(float(values[2]))
    f.close()

    E1=[]
    f = open("E_tot.dat")
    for line in f:
        if line.strip() != '':        
            values=line.strip()
            E1.append(float(values))
    f.close()

    E2=[]
    f = open("E_tot2.dat")
    for line in f:
        if line.strip() != '':        
            values=line.strip()
            E2.append(float(values))
    f.close()

   
    fig = plt.figure()
    ax=fig.add_subplot(121,projection='3d')
    ax.plot_surface(reshape(x1,(100,100)), reshape(y1,(100,100)), reshape(z1,(100,100)), cmap='viridis', edgecolor='none')
    ax2=fig.add_subplot(122,projection='3d')
    ax2.plot_surface(reshape(x2,(100,100)), reshape(y2,(100,100)), reshape(z2,(100,100)), cmap='viridis', edgecolor='none')
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('Density')
    ax.set_title('N=6 S=3')
    ax2.set_xlabel('x')
    ax2.set_ylabel('y')
    ax2.set_zlabel('Density')
    ax2.set_title('N=6 S=0')

    fig = plt.figure()
    ax=fig.add_subplot(121)
    ax.plot(E1)
    conv_cut=25
    ax.plot([conv_cut,conv_cut],plt.gca().get_ylim(),'k--')
    E1 = E1[conv_cut:]
    ax2=fig.add_subplot(122)
    ax2.plot(E2)
    conv_cut=25
    ax2.plot([conv_cut,conv_cut],plt.gca().get_ylim(),'k--')
    E2 = E2[conv_cut:]
    ax.set_xlabel('x')
    ax.set_ylabel('E_tot')
    ax.set_title('N=6 S=3')
    ax2.set_xlabel('x')
    ax2.set_ylabel('E_tot')
    ax2.set_title('N=6 S=0')

    print('Total energy(N=6,S=3): {0:.5f} +/- {1:0.5f}'.format(mean(E1), std(E1)/sqrt(len(E1))))
    print('Total energy(N=6,S=0): {0:.5f} +/- {1:0.5f}'.format(mean(E2), std(E2)/sqrt(len(E2))))

    plt.show()
    

if __name__=="__main__":
    main()