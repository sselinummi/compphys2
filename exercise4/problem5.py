"""
Exercise 4 problem 5
"""

import numpy as np
import matplotlib.pyplot as plt

def gaussian(x,l,beta):
    """
    Non-periodic free particle density matrix given by the Gaussian
    """
    return 1/np.sqrt(4*np.pi*l*beta)*np.exp(-(x**2)/(4*l*beta))


def main():
    """
    Comparing the non-periodic free partricle density martrix given by the Gaussian
    against a periodic one where periodic images of the Gaussian are summed 
    together.
    """
    x = np.linspace(-1.0,1.0,200)
    L = 2.0
    l = 0.5 #  lambda

    fig=plt.figure()
    
    # beta=0.25 and n=200 case
    beta = 0.25
    rho = 0
    # calculating the periodic density matrix
    for n in range(200):
        if n==0:
            rho=rho + gaussian(x,l,beta)
        else:
            rho=rho + gaussian(x+n*L,l,beta) + gaussian(x-n*L,l,beta)

    ax=fig.add_subplot(221)
    ax.plot(x,gaussian(x,l,beta),label='non-periodic')
    ax.plot(x,rho,'--',label='periodic')
    ax.legend()
    ax.set_title('n=200, '+r'$\beta$=0.25')

    # beta=1.0 and n=200 case
    beta = 1.0
    rho = 0
    for n in range(200):
        if n==0:
            rho=rho + gaussian(x,l,beta)
        else:
            rho=rho + gaussian(x+n*L,l,beta) + gaussian(x-n*L,l,beta)

    ax2=fig.add_subplot(222)
    ax2.plot(x,gaussian(x,l,beta),label='non-periodic')
    ax2.plot(x,rho,'--',label='periodic')
    ax2.legend()
    ax2.set_title('n=200, '+r'$\beta$=1.0')
    
    # beta=0.5 and n=10 case
    beta = 0.5
    rho = 0
    for n in range(10):
        if n==0:
            rho=rho + gaussian(x,l,beta)
        else:
            rho=rho + gaussian(x+n*L,l,beta) + gaussian(x-n*L,l,beta)

    ax3=fig.add_subplot(223)
    ax3.plot(x,gaussian(x,l,beta),label='non-periodic')
    ax3.plot(x,rho,'--',label='periodic')
    ax3.legend()
    ax3.set_title('n=10, '+r'$\beta$=0.5')
    
    # beta=0.5 and n=1000 case
    beta = 0.5
    rho = 0
    for n in range(1000):
        if n==0:
            rho=rho + gaussian(x,l,beta)
        else:
            rho=rho + gaussian(x+n*L,l,beta) + gaussian(x-n*L,l,beta)

    ax4=fig.add_subplot(224)
    ax4.plot(x,gaussian(x,l,beta),label='non-periodic')
    ax4.plot(x,rho,'--',label='periodic')
    ax4.legend()
    ax4.set_title('n=1000, '+r'$\beta$=0.5')

    plt.show()
    

if __name__=="__main__":
    main()