#! /usr/bin/env python3

"""
Hartree code for N-electron 1D harmonic quantum dot

- Related to Computational Physics 2
- Test case should give values very close to:

    Total energy      11.502873299221452
    Kinetic energy    3.6221136067112054
    Potential energy  7.880759692510247

    Density integral  4.0

- Job description: 
  -- Problem 2: add/fill needed functions, details and especially comments
  -- Problem 3: Modify parameters for calculating a different system
                as described in the problem setup
  -- Problem 4: Include input and output as text file
  -- Problem 5: Include input and output as DHF5 file

- Notice: In order to get the code running you need to add proper code sections 
  to places where it reads #FILL#
"""

import numpy as np
import matplotlib.pyplot as plt
import scipy.sparse as sp
import scipy.sparse.linalg as sla
from scipy.integrate import simps
import h5py
import os

def hartree_potential(ns,x):
    """ 
    Hartree potential using Simpson integration 
    """
    Vhartree = 0.0*ns
    for ix in range(len(x)):
        r = x[ix]
        f = 0.0*x
        for ix2 in range(len(x)):
            rp = x[ix2]
            f[ix2] = ns[ix2]*ee_potential(r-rp)
        Vhartree[ix] = simps(f,x)
    return Vhartree

def ee_potential(x):
    global ee_coef
    """ 1D electron-electron interaction """
    return ee_coef[0]/np.sqrt(x**2+ee_coef[1])

def ext_potential(x,m=1.0,omega=1.0):
    """ 1D harmonic quantum dot """
    return 0.5*m*omega**2*x**2

def density(psis):
    """ Calculate density using orbitals """
    ns=np.zeros((len(psis[0]),))
    for i in range(np.shape(psis)[0]):
        ns += abs(psis[i])**2

    return ns
    
def initialize_density(x,dx,normalization=1):
    """ some kind of initial guess for the density """
    rho = np.exp(-x**2)
    A = simps(rho,x)
    return normalization/A*rho

def check_convergence(Vold,Vnew,threshold):
    """ Check the accuracy of the result """
    difference_ = np.amax(abs(Vold-Vnew))
    print('  Convergence check:', difference_)
    converged = False
    if difference_ <threshold:
        converged = True
    return converged

def diagonal_energy(T,orbitals,x):
    """ 
    Calculate diagonal energy
    (using Simpson)
    """
    Tt = sp.csr_matrix(T)
    E_diag = 0.0
    
    for i in range(len(orbitals)):
        evec = orbitals[i]
        E_diag += simps(evec.conj()*Tt.dot(evec),x)
    return E_diag

def offdiag_potential_energy(orbitals,x):
    """ 
    Calculate off-diagonal energy
    (using Simpson)
    """
    U = 0.0
    for i in range(len(orbitals)-1):
        for j in range(i+1,len(orbitals)):
            fi = 0.0*x
            for i1 in range(len(x)):
                fj = 0.0*x
                for j1 in range(len(x)):
                    fj[j1] = abs(orbitals[i][i1])**2*abs(orbitals[j][j1])**2*ee_potential(x[i1]-x[j1])
                fi[i1] = simps(fj,x)
            U += simps(fi,x)
    return U

def save_ns_in_ascii(ns,filename):
    """ Save electron density in a text file in ascii format """
    s = ns.shape
    f = open(filename+'.txt','w')
    for ix in range(s[0]):
        f.write('{0:12.8f}\n'.format(ns[ix]))
    f.close()
    f = open(filename+'_shape.txt','w')
    f.write('{0:5}'.format(s[0]))
    f.close()
    
def load_ns_from_ascii(filename):
    """ Load electron density from an ascii formatted text file """
    f = open(filename+'_shape.txt','r')
    for line in f:
        s = np.array(line.split(),dtype=int)
    f.close()
    ns = np.zeros((s[0],))
    d = np.loadtxt(filename+'.txt')
    k = 0
    for ix in range(s[0]):
        ns[ix] = d[k]
        k += 1
    return ns


def calculate_SIC(orbitals,x):
    """ Calculate the self interaction correction """
    V_SIC = []
    for i in range(len(orbitals)):
        V_SIC.append(-hartree_potential(abs(orbitals[i])**2,x))
    return V_SIC
            
def normalize_orbital(evec,x):
    """ Normalize orbital properly """
    return evec/np.sqrt(simps(abs(evec)**2,x))

def kinetic_hamiltonian(x):
    """ Form the kinetic part of the hamiltonian """
    grid_size = x.shape[0]
    dx = x[1] - x[0]
    dx2 = dx**2
    
    H0 = sp.diags(
        [
            -0.5 / dx2 * np.ones(grid_size - 1),
            1.0 / dx2 * np.ones(grid_size),
            -0.5 / dx2 * np.ones(grid_size - 1)
        ],
        [-1, 0, 1])
    return H0

def create_hdf5_file(fname,ns,x,orbitals,N_e):
    """ Saving values into a hdf5 file """

    f = h5py.File(fname,"w")
    gset = f.create_dataset("grid",data=x,dtype='f')
    gset.attrs["info"] = '1D grid'

    eset = f.create_dataset("N_e",data=N_e,dtype='f')
    eset.attrs["info"] = 'Number of electrons'

    nset = f.create_dataset("ns",data=ns,dtype='f')
    nset.attrs["info"] = 'Density'
    
    oset = f.create_dataset("orbitals",shape=(len(x),N_e),dtype='f')
    oset.attrs["info"] = '1D orbitals as (len(grid),N_electrons)'
    for i in range(len(orbitals)):
        oset[:,i]=orbitals[i]
    
    f.close()

def read_hdf5_file(fname):
    """ Loading values from the hdf5 file """

    f = h5py.File(fname,"r")
    print('Keys in hdf5 file: ',list(f.keys()))
    x = np.array(f["grid"])
    orbs = np.array(f["orbitals"])
    ns = np.array(f["ns"])
    N_e = int(np.array(f["N_e"]))
    orbitals = []
    for i in range(len(orbs[0,:])):
        orbitals.append(orbs[:,i])
    f.close()
    return x,N_e,ns,orbitals

def main():
    global ee_coef
    # e-e potential parameters [strenght, smoothness]
    ee_coef = [1.0, 1.0]
    # 1D occupations each orbital can have max 2, i.e., spin up and spin down
    # e.g., occ = [0,0,1,1] means lowest up and down, next lowest up and down
    #       occ = [0,1,2,3] means all spin up
    
    occ = [0,1,2,3] # test case (i.e., P2)

    # number of electrons
    N_e = len(occ)
    x = np.linspace(-4,4,120)
    dx = x[1]-x[0]
    orbitals = []
    ns = initialize_density(x,dx,N_e)

    # grid
    #x = np.linspace(-4,4,120)
    # threshold
    threshold = 1.0e-5
    # mixing value
    mix_alpha = 0.2
    # maximum number of iterations
    maxiters = 100
    
    # READ in density / orbitals / etc. (related to problems 3 and 4)
    VSIC = []
    if os.path.isfile('data.hdf5'):
        x,N_e,ns,orbitals = read_hdf5_file("data.hdf5")
        VSIC =  calculate_SIC(orbitals,x)
    else:  
        for i in range(N_e):
            VSIC.append(ns*0.0)

    dx = x[1]-x[0]
    T = kinetic_hamiltonian(x)
    Vext = ext_potential(x)

    print('Density integral        ', simps(ns,x))
    print(' -- should be close to  ', N_e)
    
    print('\nCalculating initial state')
    Vhartree = hartree_potential(ns,x)


    # Now VSIC is initialized as zero, since there are no orbitals yet
    # - This is good for problem 2
    # - In problems 3 and 4 you need to be able to fill it with data from
    #   prior calculation
    

    Veff = sp.diags(Vext+Vhartree,0)
    H = T+Veff
    # performing the SCF method until the maximum number of iterations
    # has been reached or the wave function is converged
    for i in range(maxiters):
        print('\n\nIteration #{0}'.format(i))
        orbitals = []
        # calculating the orbitals
        for i in range(N_e):
            print('  Calculating orbitals for electron ', i+1)
            eigs, evecs = sla.eigs(H+sp.diags(VSIC[i],0), k=N_e, which='SR')
            eigs = np.real(eigs)
            evecs = np.real(evecs)
            print('    eigenvalues', eigs)
            evecs[:,occ[i]] = normalize_orbital(evecs[:,occ[i]],x)
            orbitals.append(evecs[:,occ[i]])
        Veff_old = 1.0*Veff
        ns = density(orbitals)
        Vhartree = hartree_potential(ns,x)
        VSIC = calculate_SIC(orbitals,x)
        Veff_new = sp.diags(Vext+Vhartree,0)
        if check_convergence(Veff_old,Veff_new,threshold):
            break
        else:
            """ Mixing the potential, mix_alpha is the mixing parameter """
            Veff = mix_alpha*Veff_old+(1-mix_alpha)*Veff_new
            H = T+Veff

    print('\n\n')
    # calculating kinectic, potential and total energies
    off = offdiag_potential_energy(orbitals,x)
    E_kin = diagonal_energy(T,orbitals,x)
    E_pot = diagonal_energy(sp.diags(Vext,0),orbitals,x) + off
    E_tot = E_kin + E_pot
    print('Total energy     ', E_tot)
    print('Kinetic energy   ', E_kin)
    print('Potential energy ', E_pot) 
    print('\nDensity integral ', simps(ns,x))

    # WRITE OUT density / orbitals / energetics / etc. (related to problems 3 and 4)
    create_hdf5_file("data.hdf5",ns,x,orbitals,N_e)
    

    
    # for plotting... use tex if available
    """
    try:
        plt.rcParams['text.usetex'] = True
    except:
        pass
    """
    # visualizing the results
    plt.rcParams['font.size'] = 12
    plt.rcParams['legend.handlelength'] = 2 
    plt.rcParams['legend.numpoints'] = 1
    plt.plot(x,abs(ns))
    plt.xlabel(r'$x$ (a.u.)')
    plt.ylabel(r'$n(x)$ (1/a.u.)')
    plt.title(r'$N$-electron density for $N={0}$'.format(N_e))
    plt.text(-2.0,0.3,r'Total energy: {0:.6f}'.format(E_tot))
    plt.text(-2.0,0.2,r'Kinetic energy: {0:.6f}'.format(E_kin))
    plt.text(-2.0,0.1,r'Potential energy: {0:.6f}'.format(E_pot))
    plt.show()

if __name__=="__main__":
    main()