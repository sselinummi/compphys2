#!/bin/bash

################################################################
## * This script builds available configurations of QMCPACK   ##
##   on Puhti at CSC                                          ##
##                                                            ##
## * Execute this script in trunk/                            ##
##   ./config/build_csc_puhti_complex_only.sh                 ##
##                                                            ##
## Last modified: April, 2022                                 ##
################################################################

module purge
module load intel-oneapi-compilers 
module load intel-oneapi-mpi 
module load intel-oneapi-mkl 
module load fftw 
module load boost
module load hdf5/1.10.7-mpi
module load python-env
module load cmake

GCC_CONF="-gnu-prefix=/appl/spack/install-tree/gcc-4.8.5/gcc-8.3.0-qzmzn5/bin/ -Xlinker -rpath=/appl/spack/install-tree/gcc-4.8.5/gcc-8.3.0-qzmzn5/lib64"

GCC_CFG="$(pwd)/config/gcc_puhti_conf.cfg"
echo $GCC_CONF > $GCC_CFG

export ICCCFG=$GCC_CFG
export ICPCCFG=$GCC_CFG
export IFORTCFG=$GCC_CFG

CMAKE_FLAGS="-DCMAKE_C_COMPILER=mpiicc \ 
             -DCMAKE_CXX_COMPILER=mpiicpc \
             -DLIBXML2_LIBRARY=/usr/lib64/libxml2.so"

# Configure and build cpu complex 
echo ""
echo ""
echo "building complex qmcpack for cpu on Puhti"
mkdir -p build_csc_puhti_complex_only
cd build_csc_puhti_complex_only
cmake -DQMC_COMPLEX=1 $CMAKE_FLAGS ..
make -j 12 
cd ..
