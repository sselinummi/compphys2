"""
Script for plotting the energies that were calculated at CSC
"""

from numpy import *
import matplotlib.pyplot as plt

def main():
    # Energies of VMC without Jastrow, VMC with optimized Jastrow 
    # and DMC with two different time steps
    energies=[-87.818743,-90.290355,-90.715656,-90.926062]
    methods=['VMC','VMC+J','DMC1','DMC2']
    # Total energies at different stages in optimization
    energies_opt=[-89.841670,-90.208448,-90.269411,-90.272749,-90.297874,-90.284541,-90.289176,-90.262980,-90.260523,-90.290355]
    plt.figure()
    plt.plot(methods,energies)
    plt.xlabel('Stage')
    plt.ylabel('Energy')
    plt.title('Energies during the whole procedure')
    plt.figure()
    plt.plot(energies_opt)
    plt.xlabel('Stage')
    plt.ylabel('Energy')
    plt.title('Total energy during the optimization')
    plt.show()

if __name__=="__main__":
    main()