"""
Simple VMC and DMC code for Computational Physics 2 course at TAU

- Fill in more comments based on lecture slides
- Follow assignment instructions
-- e.g., the internuclear distance should be 1.4 instead of 1.5
   and currently a=1, but you should use 1.1 at first


By Ilkka Kylanpaa
"""

from numpy import *
import matplotlib.pyplot as plt

class Walker:

    def __init__(self,*args,**kwargs):
        self.Ne = kwargs['Ne']
        self.Re = kwargs['Re']
        self.spins = kwargs['spins']
        self.Nn = kwargs['Nn']
        self.Rn = kwargs['Rn']
        self.Zn = kwargs['Zn']
        self.sys_dim = kwargs['dim']

    def w_copy(self):
        return Walker(Ne=self.Ne,
                      Re=self.Re.copy(),
                      spins=self.spins.copy(),
                      Nn=self.Nn,
                      Rn=self.Rn.copy(),
                      Zn=self.Zn,
                      dim=self.sys_dim)
    

def vmc_run(Nblocks,Niters,Delta,Walkers_in,Ntarget):
    Eb = zeros((Nblocks,))
    Accept = zeros((Nblocks,))

    vmc_walker = Walkers_in[0] # just one walker needed
    Walkers_out = []
    for i in range(Nblocks):
        for j in range(Niters):
            # moving only electrons
            for k in range(vmc_walker.Ne):
                R = vmc_walker.Re[k].copy()
                Psi_R = wfs(vmc_walker)

                # move the particle
                vmc_walker.Re[k] = R + Delta*(random.rand(vmc_walker.sys_dim)-0.5)
                
                # calculate wave function at the new position
                Psi_Rp = wfs(vmc_walker)

                # calculate the sampling probability
                A_RtoRp = min((Psi_Rp/Psi_R)**2,1.0)
                
                # Metropolis
                if (A_RtoRp > random.rand()):
                    Accept[i] += 1.0/vmc_walker.Ne
                else:
                    vmc_walker.Re[k] = R
                #end if
            #end for
            Eb[i] += E_local(vmc_walker)
        #end for
        if (len(Walkers_out)<Ntarget):
            Walkers_out.append(vmc_walker.w_copy())
        Eb[i] /= Niters
        Accept[i] /= Niters
        print('Block {0}/{1}'.format(i+1,Nblocks))
        print('    E   = {0:.5f}'.format(Eb[i]))
        print('    Acc = {0:.5f}'.format(Accept[i]))
    
    return Walkers_out, Eb, Accept


def dmc_run(Nblocks,Niters,Walkers,tau,E_T,Ntarget):
    
    max_walkers = 2*Ntarget
    lW = len(Walkers)
    while len(Walkers)<Ntarget:
        Walkers.append(Walkers[max(1,int(lW*random.rand()))].w_copy())

    Eb = zeros((Nblocks,))
    Accept = zeros((Nblocks,))
    AccCount = zeros((Nblocks,))
    
    obs_interval = 5 # defines how often observables will be calculated
    mass = 1

    for i in range(Nblocks):
        EbCount = 0
        for j in range(Niters):
            Wsize = len(Walkers)
            Idead = []
            for k in range(Wsize):
                Acc = 0.0
                # moving only electrons
                for i_np in range(Walkers[k].Ne):
                    # old position
                    R = Walkers[k].Re[i_np].copy()
                    # wave function at R
                    Psi_R = wfs(Walkers[k])
                    # drift term at R
                    DriftPsi_R = 2*Gradient(Walkers[k],i_np)/Psi_R*tau/2/mass
                    E_L_R = E_local(Walkers[k])

                    DeltaR = random.randn(Walkers[k].sys_dim)
                    logGf = -0.5*dot(DeltaR,DeltaR)
                    
                    # move the particle
                    Walkers[k].Re[i_np] = R+DriftPsi_R+DeltaR*sqrt(tau/mass)
                    
                    # calculate wave function at the new position
                    Psi_Rp = wfs(Walkers[k])
                    DriftPsi_Rp = 2*Gradient(Walkers[k],i_np)/Psi_Rp*tau/2/mass
                    E_L_Rp = E_local(Walkers[k])
                    
                    DeltaR = R-Walkers[k].Re[i_np]-DriftPsi_Rp
                    logGb = -dot(DeltaR,DeltaR)/2/tau*mass

                    # calculate the sampling probability
                    A_RtoRp = min(1, (Psi_Rp/Psi_R)**2*exp(logGb-logGf))

                    # Metropolis
                    if (A_RtoRp > random.rand()):
                        Acc += 1.0/Walkers[k].Ne
                        Accept[i] += 1
                    else:
                        Walkers[k].Re[i_np] = R
                    
                    AccCount[i] += 1
                
                # calculating the effective time step
                tau_eff = Acc*tau
                # branching term of the propagator
                GB = exp(-(0.5*(E_L_R+E_L_Rp) - E_T)*tau_eff)
                MB = int(floor(GB + random.rand()))
                
                if MB>1:
                    for n in range(MB-1):
                        if (len(Walkers) < max_walkers):
                            Walkers.append(Walkers[k].w_copy())
                elif MB==0:
                    Idead.append(k)
 
            Walkers = DeleteWalkers(Walkers,Idead)

            # Calculate observables every now and then
            if j % obs_interval == 0:
                EL = Observable_E(Walkers)
                Eb[i] += EL
                EbCount += 1
                E_T += 0.01/tau*log(Ntarget/len(Walkers))
                

        # Keep the target number of walkers
        Nw = len(Walkers)
        dNw = Ntarget-Nw
        for kk in range(abs(dNw)):
            ind = int(floor(len(Walkers)*random.rand()))
            if (dNw>0):
                Walkers.append(Walkers[ind].w_copy())
            elif dNw<0:
                Walkers = DeleteWalkers(Walkers,[ind])

        
        Eb[i] /= EbCount
        Accept[i] /= AccCount[i]
        print('Block {0}/{1}'.format(i+1,Nblocks))
        print('    E   = {0:.5f}'.format(Eb[i]))
        print('    Acc = {0:.5f}'.format(Accept[i]))


    return Walkers, Eb, Accept

def DeleteWalkers(Walkers,Idead):
    if (len(Idead)>0):
        if (len(Walkers)==len(Idead)):
            Walkers = Walkers[0]
        else:
            Idead.sort(reverse=True)   
            for i in range(len(Idead)):
                del Walkers[Idead[i]]

    return Walkers

def H_1s(r1,r2):
    # exponential term in the trial wave function
    return exp(-(a)*sqrt(sum((r1-r2)**2)))
     
def wfs(Walker):
    # Forms the wave function of a walker
    
    # H2 approx
    f = H_1s(Walker.Re[0],Walker.Rn[0])+H_1s(Walker.Re[0],Walker.Rn[1])
    f *= (H_1s(Walker.Re[1],Walker.Rn[0])+H_1s(Walker.Re[1],Walker.Rn[1]))

    J = 0.0
    # Jastrow e-e
    for i in range(Walker.Ne-1):
        for j in range(i+1,Walker.Ne):
           r = sqrt(sum((Walker.Re[i]-Walker.Re[j])**2))
           if (Walker.spins[i]==Walker.spins[j]):
               J += 0.25*r/(1.0+1.0*r)
           else:
               J += 0.5*r/(1.0+1.0*r)
    
    # Jastrow e-Ion
    for i in range(Walker.Ne):
        for j in range(Walker.Nn):
           r = sqrt(sum((Walker.Re[i]-Walker.Rn[j])**2))
           J -= Walker.Zn[j]*r/(1.0+b1*r) 

    # for testing VMC without Jastrow
    if no_Jastrow==True:
        J=0.0
       

    return f*exp(J)

def potential(Walker):
    # Calculates the potetial energy of a walker consisting of
    # an electron-electron, electorn-ion and ion-ion interactions
    V = 0.0
    r_cut = 1.0e-4
    # e-e
    for i in range(Walker.Ne-1):
        for j in range(i+1,Walker.Ne):
            r = sqrt(sum((Walker.Re[i]-Walker.Re[j])**2))
            V += 1.0/max(r_cut,r)

    # e-Ion
    for i in range(Walker.Ne):
        for j in range(Walker.Nn):
            r = sqrt(sum((Walker.Re[i]-Walker.Rn[j])**2))
            V -= Walker.Zn[j]/max(r_cut,r)

    # Ion-Ion
    for i in range(Walker.Nn-1):
        for j in range(i+1,Walker.Nn):
            r = sqrt(sum((Walker.Rn[i]-Walker.Rn[j])**2))
            V += 1.0/max(r_cut,r)

    return V

def Local_Kinetic(Walker):
    # Calculates the local kinetic energy of a walker
    # laplacian -0.5 \nabla^2 \Psi / \Psi
    h = 0.001
    h2 = h*h
    K = 0.0
    Psi_R = wfs(Walker)
    for i in range(Walker.Ne):
        for j in range(Walker.sys_dim):
            Y=Walker.Re[i][j]
            Walker.Re[i][j]-=h
            wfs1 = wfs(Walker)
            Walker.Re[i][j]+=2.0*h
            wfs2 = wfs(Walker)
            K -= 0.5*(wfs1+wfs2-2.0*Psi_R)/h2
            Walker.Re[i][j]=Y
    return K/Psi_R

def Gradient(Walker,particle):
    h=0.001
    dPsi = zeros(shape=shape(Walker.Re[particle]))
    for i in range(Walker.sys_dim):
        Y=Walker.Re[particle][i]
        Walker.Re[particle][i]-=h
        wfs1=wfs(Walker)
        Walker.Re[particle][i]+=2.0*h
        wfs2=wfs(Walker)
        dPsi[i] = (wfs2-wfs1)/2/h
        Walker.Re[particle][i]=Y

    return dPsi

def E_local(Walker):
    # for calculating the local energy
    return Local_Kinetic(Walker)+potential(Walker)

def Observable_E(Walkers):
    # for calculating local energy observable
    E=0.0
    Nw = len(Walkers)
    for i in range(Nw):
        E += E_local(Walkers[i])
    E /= Nw
    return E

def main():
    global a
    global b1
    global no_Jastrow
    no_Jastrow=True # for disabling Jastrow factor
    b1=100.0
    Walkers=[]
    Walkers.append(Walker(Ne=2, # the number of electrons
                          Re=[array([0.5,0,0]),array([-0.5,0,0])], # electron locations
                          spins=[0,1], 
                          Nn=2, # the number of nuclei
                          Rn=[array([-0.7,0,0]),array([0.7,0,0])], # nuclei locations
                          Zn=[1.0,1.0],
                          dim=3))

    # for deciding if DMC will be calculated as well
    vmc_only = False

    Ntarget=100
    vmc_time_step = 2.5 # 8.0 was default

    Ebs=[]
    ratios=[]
    e_errors=[]
    a_list=[1.1,1.2,1.3,1.4,1.5]
    # performing VMC with different a values to find the one
    # with the lowest energy
    for a_i in a_list:
        a=a_i
        Walkers, Eb, Acc = vmc_run(100,50,vmc_time_step,Walkers,Ntarget)
        print('\nMean acceptance ratio: {0:0.3f} +/- {1:0.3f}'.format(mean(Acc),std(Acc)/sqrt(len(Acc))))
        print('VMC total energy: {0:0.5f} +/- {1:0.5f}'.format(mean(Eb), std(Eb)/sqrt(len(Eb))))
        print('Variace to energy ratio(abs): {0:0.5f}'.format(abs(std(Eb)/mean(Eb))))
        Ebs.append(mean(Eb))
        ratios.append(abs(std(Eb)/mean(Eb)))
        e_errors.append(std(Eb)/sqrt(len(Eb)))

    # visualizing energies and variance to energy ratios
    fig=plt.figure()
    ax=fig.add_subplot(211)
    ax.errorbar(a_list, Ebs,e_errors,ecolor='red')
    ax2=fig.add_subplot(212)
    ax2.plot(a_list,ratios)
    plt.show()

    #a=0.5 for poor wave function
    #b1=0.5 for poor wave function
    a=1.20 # value from a_list that gives the minimum energy
    if not vmc_only:
        Walkers, Eb_dmc, Accept_dmc = dmc_run(10,10,Walkers,0.05,mean(Eb),Ntarget)
    print('VMC and DMC total energies: {0:0.5f} and {1:0.5f}'.format(mean(Ebs[2]), mean(Eb_dmc)))
    
if __name__=="__main__":
    main()
        
